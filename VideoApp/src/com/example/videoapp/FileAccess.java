package com.example.videoapp;

/*
 * The class for handling file system related works.
 * There are some file system settings.
 * Need to confirm the permission on tablet
 */

/**
 * @author Helei Wang
 */

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.Time;
import android.util.Log;

public class FileAccess {
	public final static String ROOT_DIR = "/storage/emulated/0/videoapp/";
	public final static String STORAGE_DIR = ROOT_DIR + "Pictures/"; 
	public final static String TEMP_DIR = ROOT_DIR + "Temp/";
	public final static String TEMP_DIR_PHOTO = TEMP_DIR + "photos/";
	public static final String TEMP_DIR_VIDEO = TEMP_DIR + "videos/";
	public final static String SETTING_FILE = ROOT_DIR + "setting/preference.txt";
	public static final String[] videoLocations = { TEMP_DIR_VIDEO + "temp0.mp4",
												TEMP_DIR_VIDEO + "temp1.mp4",
												TEMP_DIR_VIDEO + "temp2.mp4"};
	private static final FileAccess INSTANCE = new FileAccess();
	 
	private FileAccess() {
		File dir = new File(ROOT_DIR);
		if(!dir.exists()){
			 dir.mkdirs();
		}
		
		File dir0 = new File(TEMP_DIR);
		if(!dir0.exists()){
			 dir0.mkdirs();
		}
		
		File dir1 = new File(TEMP_DIR_PHOTO);
		if(!dir1.exists()){
			 dir1.mkdirs();
		}
		
		File dir2 = new File(TEMP_DIR_VIDEO);
		if(!dir2.exists()){
			 dir2.mkdirs();
		}
		
		File dir3 = new File(STORAGE_DIR);
		if(!dir3.exists()){
			 dir3.mkdirs();
		}
		
		File settingfile = new File(SETTING_FILE);
		if(!settingfile.exists()){
			//TODO
		} 
	}
	 
	public static FileAccess getInstance() {
	     return INSTANCE;
	}
	 
	public void cleanTempPhotoDir(){
		 File file = new File(TEMP_DIR_PHOTO);
		 if(!file.exists()){
			 file.mkdirs();
			 return;
		 }
			
		 if (file.isDirectory()){
		        for (File child : file.listFiles())
		        {
		            child.delete();
		        }
	      }
	}
	
	public void cleanTempVideoDir(){
		 File file = new File(TEMP_DIR_VIDEO);
		 if(!file.exists()){
			 file.mkdirs();
			 return;
		 }
			
		 if (file.isDirectory()){
		        for (File child : file.listFiles())
		        {
		            child.delete();
		        }
	      }
	}
	
	public void cleanTempDir(){
		cleanTempPhotoDir();
		cleanTempVideoDir();
	}
	 
	public ArrayList<String> getPhotosByDate(int year, int month, int day, int limit){
		String dirStr = STORAGE_DIR + year + "-" + month + "-"+ day + "/";
		ArrayList<String> result = new ArrayList<String>();
		
		File dir = new File(dirStr);
		if(!dir.exists() || limit < 0){
			 return result;
		}
		if (dir.isDirectory()){
	        for (File child : dir.listFiles())
	        {    
	        	String s = child.getPath();
	            result.add(s);
	        }
        }
		
		Collections.sort(result);
		Collections.reverse(result);
		
		List<String> subResult = new ArrayList<String>();
		if(limit != 0 && result.size() > limit){
			subResult =  result.subList(0, limit);
		}
		else{
			subResult = result;
		}
		
		for (String s : result){
			Log.d("result", s);
		}
		
		ArrayList<String> returnResult = new ArrayList<String>();
		returnResult.addAll(subResult);
		return returnResult;
	}
	
	public boolean saveImage(String imgPath){
		File file = createFile();
		Bitmap bmp = null;
	    try {
	    	/* Copy the tmp file over */
	    	bmp = BitmapFactory.decodeFile(imgPath);
	        FileOutputStream out = new FileOutputStream(file);
	        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	        out.flush();
	        out.close();
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	        if(bmp != null){
	        	bmp.recycle();
	        }
	        return false;
	    }
	    bmp.recycle();
		return true;
	}
	
	private File createFile(){
		/* Format saving folder and fileName */
		Time now = new Time();
		now.setToNow();
		String folderName = now.year + "-" + (now.month+1) + "-" + now.monthDay;
		String fileName = (now.hour<10?"0":"") + now.hour + "-" + (now.minute<10?"0":"") + now.minute + "-" 
							+ (now.second<10?"0":"") + now.second + ".jpg";
		
		/* Create the file */
		File dir = new File (STORAGE_DIR, folderName);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		File file = new File (dir, fileName);
	    if (file.exists ()) file.delete (); 
		return file;
	}
		
	public boolean saveBitmap(Bitmap bmp){
		System.gc();
		File file = createFile();
	    try {
	    	/* Copy the tmp file over */
	        FileOutputStream out = new FileOutputStream(file);
	        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	        out.flush();
	        out.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	        return false;
	    }
		return true;
	}
	 
}
