package com.example.videoapp;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

/*
 * The class for selecting one photos of snapshot result. 
 * It is the second activity of recording mode.
 */

/**
 * @author Helei Wang
 * Jianghao Lu
 */

public class OneImageGalleryActivity extends Activity {	
	private int current = -1;
	private final int[] imageviewIDs = {R.id.one_image_0, R.id.one_image_1,R.id.one_image_2,R.id.one_image_3,R.id.one_image_4,R.id.one_image_5,
			R.id.one_image_6,R.id.one_image_7,R.id.one_image_8,R.id.one_image_9};
    private ArrayList<String> photos;    
    private Timer scanTimer;
    private Boolean runFirst = true;
    
    private boolean isLocked = true;
	private Timer lockTimer = new Timer();

	private void saveImage(String imgPath) {
		FileAccess fileAccess = FileAccess.getInstance();
		boolean ifSave = fileAccess.saveImage(imgPath);
		if(ifSave){
			Context context = getApplicationContext();
			CharSequence text = "You have saved a photo.";
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}
	}
    
    private void saveAndReturn(String imgPath) {
    	saveImage(imgPath);
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		System.gc();
		finish();
    }
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one_image_gallery);
		
		lockTimer.schedule(new unLockTask(), 1000);
		
		Intent intent = getIntent();
		photos = intent.getStringArrayListExtra("snapshotPhotos");		
		
		if(photos == null || photos.size() != 10){
			Intent emptyIntent  = new Intent(this, OneEmptyActivity.class);
	    	startActivity(emptyIntent);
	    	System.gc();
	    	finish();
		}
		
		for(int index = 0; index < photos.size(); ++index){
			//File pic = new File(photos.get(index));
			ImageView img= (ImageView) findViewById(imageviewIDs[index]); 
			
			//img.setImageURI(Uri.fromFile(pic));
			img.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeFile(photos.get(index)), 256, 144, true));
		}
		
		scanTimer = new Timer();
		scanTimer.schedule(new ScanImageTask(), 1000, 3000);//3 sec
	}
	
	@Override
	public void onBackPressed() {}
	
	class unLockTask extends TimerTask {		
		public void run() {			
			isLocked = false;
			lockTimer.cancel();
		}
    }
	
	public void jumpToMain(View view) {
		if (!isLocked) {
			screenOnClick(view);
		}
	}
	
	public void screenOnClick(View view) {
		scanTimer.cancel();
		if(current < 0 || current > photos.size()*2 -1){
			returnMainActivity();
		}
		saveAndReturn(photos.get(current%10));
	}
	
	private void returnMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);        
        startActivity(intent);
        System.gc();
        finish();
	}
	
	class ScanImageTask extends TimerTask {
	      public void run() {
	    	  if(current == photos.size()-1 ){ //eg current == 9
	    		  if(runFirst){
	    			  
	    			  runOnUiThread(new Runnable() {
	    				  public void run() {
	    					  Context context = getApplicationContext();
	    	    			  CharSequence text = "Last loop. Select the photo you want to save!";
	    	    			  int duration = Toast.LENGTH_SHORT;
	    	    			  Toast toast = Toast.makeText(context, text, duration);
	    	    			  toast.show();
	    	    			//loop again
	    	    			  ImageView lastImg= (ImageView) findViewById(imageviewIDs[current]); 
	        		    	  lastImg.setBackgroundColor(0xFF000000);
	        		    	  
	    				  }
	    			  });
    		    	  runFirst = false;
    		    	  scanTimer.cancel();
    		    	  scanTimer = new Timer();
    		  		  scanTimer.schedule(new ScanImageTask(), 1000, 3000);//3 sec
	    			  return;
	    		  }
	    	  }
	    	  else if (current == photos.size()*2-1){
	    			  runOnUiThread(new Runnable() {
	    				  public void run() {
	    					  Context context = getApplicationContext();
	    	    			  CharSequence text = "You did not select a photo.";
	    	    			  int duration = Toast.LENGTH_SHORT;
	    	    			  Toast toast = Toast.makeText(context, text, duration);
	    	    			  toast.show();
	    				  }
	    			  });
	    			  
	    			  scanTimer.cancel();
		    	      returnMainActivity();
		    	      return;	    		  
	    	  }
	    	  ++current;
	    	  
	    	  runOnUiThread(new Runnable() 
	          {
	              public void run() 
	              {
	            	  ImageView img= (ImageView) findViewById(imageviewIDs[current%10]); 
	    	    	  img.setBackgroundColor(0xFFFF0000);//red
	    	    	  ImageView bigImg = (ImageView) findViewById(R.id.one_image_big);
	    	    	  File pic = new File(photos.get(current%10));
	    	    	  bigImg.setImageURI(Uri.fromFile(pic));
	    	    	  if(current%10 > 0){
	    	    		  ImageView lastImg= (ImageView) findViewById(imageviewIDs[current%10-1]); 
	    		    	  lastImg.setBackgroundColor(0xFF000000);//black
	    	    	  }
	              }
	          });
	      }
	}
}