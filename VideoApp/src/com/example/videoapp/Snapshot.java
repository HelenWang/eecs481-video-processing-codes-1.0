package com.example.videoapp;

/*
 * The class is for recording page. 
 * It is not an activity of recording mode.
 * It is a back-end calculation class.
 * It will be used by RecordingModeActivity class when user clicks on snapshot button
 */

/**
 * @author Helei Wang
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.util.Log;

@SuppressLint("NewApi")
public class Snapshot{
	private ArrayList<String> uris;
	private Queue<MediaMetadataRetriever> retrievers;
	/*This queue stores recorded videos in the order that 
	  the latest one is at the end of the queue */ 
	private Queue<Long> times;
	//This queue stores corresponding length of videos in retrievers
	private int tp[] = { 500,1500,2500,3500,4500,5500,6500,7500,8500,9500}; //10 pics
	
	private FileAccess fileAccess = FileAccess.getInstance();
	public Snapshot(){}	
	
	@SuppressLint("NewApi")
	public boolean sign(String[] locations, int last){
		//return false if cannot get five photos  
	
		//first check if all three files exist
		uris = new ArrayList<String>();
		for(int i = 0; i<locations.length;i++){
			String uri = locations[(last+locations.length-i)%locations.length];
			File file = new File(uri);
			if(file.exists()){
				Log.d("test","exist"+uri);
				uris.add(uri);
			}
		}
		
		retrievers = new LinkedList<MediaMetadataRetriever>();
		times = new LinkedList<Long>();
		long timeTotal = 0;
		for(String uri : uris){
			MediaMetadataRetriever  retriever = new MediaMetadataRetriever();
			retriever.setDataSource(uri);
			String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
			long timeInmillisec = Long.parseLong( time );
			
			retrievers.add(retriever);
			times.add(timeInmillisec);
			
			Log.d("test","timeInmillisec"+uri+" "+timeInmillisec);
			
			timeTotal += timeInmillisec;
			if(timeTotal >= 10*1000){
				System.gc();
				return true;//stop add retrievers
			}
		}
		
		Log.d("test","timeTotal" + timeTotal);
		
		if(timeTotal <= 10*1000){
			System.gc();
			return false;//must longer than 20sec
		}
		else{
			System.gc();
			return true;
		}
	}	
	
	private void clean(){
		System.gc();
		FileAccess fileAccess = FileAccess.getInstance();
		fileAccess.cleanTempPhotoDir();
	}
	
	public ArrayList<String> getPhotos(){
		clean();
		ArrayList<String> photos = new ArrayList<String>();
		MediaMetadataRetriever  retriever = retrievers.poll();
		int cur = 0;
		Long timehistory = (long) 0;
		while(retriever != null){
			Long time = times.poll();
			while(time > tp[cur]-timehistory){
				//cut a picture
				Bitmap bmFrame = retriever.getFrameAtTime(1000*(time-(tp[cur]-timehistory))); //unit in microsecond
				Log.d("test","time"+(tp[cur]-timehistory));
				String path = fileAccess.TEMP_DIR_PHOTO;
				OutputStream fOut = null;
				String name = "pic"+cur+".jpg";
				File file = new File(path, name);
				
				try {
					fOut = new FileOutputStream(file);
					bmFrame.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
					fOut.flush();
					fOut.close();
					bmFrame.recycle();
					System.gc();
				       
				} catch (Exception e) {
				       e.printStackTrace();
				}
				
				photos.add(path + name);
				
				++cur;
				if(cur == tp.length){
					cleanRetrievers();
					return photos;
				}
			}
			timehistory += time;
			retriever.release();
			retriever = retrievers.poll();
		}		
		cleanRetrievers();
		System.gc();
		return photos;//never here
	}	
	
	private void cleanRetrievers(){
		if(retrievers.size() > 0){
			MediaMetadataRetriever  retriever = retrievers.poll();
			while(retriever != null){
				retriever.release();
				retriever = retrievers.poll();
			}
		}
	}
}