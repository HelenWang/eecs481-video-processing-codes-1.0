package com.example.videoapp;

/*
 * The class is used special case that snapshot cannot extract proper photos. 
 * It is an alert page and will return to home page auto.
 */

/**
 * @author Helei Wang
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class OneEmptyActivity extends Activity {
	 private Handler mHandler = new Handler();
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one_empty);
		mHandler.postDelayed(new Runnable() {
            public void run() {
                jump();
            }
        }, 3000);
	}
	
	private void jump(){
		Intent intent  = new Intent(this, MainActivity.class);
    	startActivity(intent);
    	System.gc();
    	finish();
	}
}
