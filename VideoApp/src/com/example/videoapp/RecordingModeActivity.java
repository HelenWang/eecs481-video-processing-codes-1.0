package com.example.videoapp;

/*
 * The class is for recording page. 
 * It is the first activity of recording mode.
 * It will use snapshot class when user clicks on snapshot button
 */

/**
 * @author Helei Wang
 * Xisheng Yao
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class RecordingModeActivity extends Activity implements SurfaceHolder.Callback{
	private MediaRecorder mediarecorder = null;
	private SurfaceHolder surfaceHolder;
	private SurfaceView surfaceview;
	private Timer recordTimer;
	private Timer stopButtonTimer;
	private Timer snapshotButtonTimer;
	private enum Records { First, Second, Third };
	private Records nextRecord = Records.First;	
	private boolean isOpen;
	private FileAccess fileAccess = FileAccess.getInstance();
	private ImageButton buttonStop;
	private ImageButton buttonSnapshot;
	private boolean firstShoot = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recording_mode);

		//disable two buttons		
		int resID = getResources().getIdentifier("buttonStop", "id", "com.example.videoapp");
        buttonStop = (ImageButton) findViewById(resID);
        buttonStop.setEnabled(false);
        resID = getResources().getIdentifier("buttonSnapshot", "id", "com.example.videoapp");
        buttonSnapshot = (ImageButton) findViewById(resID);
        buttonSnapshot.setEnabled(false);
         
		isOpen = true;
		recordTimer = new Timer();
		this.clean();
		stopButtonTimer = new Timer();
		snapshotButtonTimer = new Timer();
		surfaceview = (SurfaceView) this.findViewById(R.id.surfaceview);  
		SurfaceHolder holder = surfaceview.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); 
	}
	
	@Override
	public void onBackPressed() {}
	
	@Override 
	public void onStop() {
		super.onStop();		
		recordTimer.cancel();
		stopButtonTimer.cancel();
		snapshotButtonTimer.cancel();
		stop();
		clean();
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		stop();
		Intent intent = new Intent(this, MainActivity.class);		
        startActivity(intent);
        System.gc();
        finish();
	}
	
	class RollTask extends TimerTask {
	      public void run() {
	    	  System.gc();
	    	  shoot();
	      }
	}
	
	class stopButtonTask extends TimerTask {
	      public void run() {
	    	  enableButton("buttonStop", true);  
	      }
	}
	
	class snapshotButtonTask extends TimerTask {
	      public void run() {
	    	  enableButton("buttonSnapshot", true); 
	      }
	}
	
	public void onClickStop(View view){
		recordTimer.cancel();
		stopButtonTimer.cancel();
		snapshotButtonTimer.cancel();
		enableButton("buttonStop", false);
		enableButton("buttonSnapshot", false);
		this.stop();
		this.clean();
		Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        System.gc();
        finish();
	}	

	public void onClickSnapshot(View view){
		recordTimer.cancel();
		stopButtonTimer.cancel();
		snapshotButtonTimer.cancel();
		enableButton("buttonStop", false);
		enableButton("buttonSnapshot", false);
		stop();
		
		ArrayList<String> photos = new ArrayList<String>();
		photos = this.snapshot();
		Intent intent = null;
		if(photos == null || photos.size() != 10){ //hardcode
			intent = new Intent(this,OneEmptyActivity.class);
		}
		else{
			intent = new Intent(this,OneImageGalleryActivity.class);
			intent.putStringArrayListExtra("snapshotPhotos", photos);
		}
		clean();

        startActivity(intent);	
        System.gc();
        finish();
	}	
	
	private synchronized void shoot(){
		if(!firstShoot){
			stopButtonTimer.cancel();
			snapshotButtonTimer.cancel();
		}
		this.tempStop();
		
		boolean success = this.start();
		if (!success) {
			recordTimer.cancel();
			Intent intent = new Intent(this,MainActivity.class);
			startActivity(intent);
			System.gc();
			finish();
		}
		
		if(!firstShoot){
			stopButtonTimer = new Timer();
			snapshotButtonTimer = new Timer();
			stopButtonTimer.schedule(new stopButtonTask(), 1200); //1.2sec
			snapshotButtonTimer.schedule(new snapshotButtonTask(),1200);//1.2
		}
		if(firstShoot){
			firstShoot = false;
		}
	}
	
	private synchronized boolean start(){
		if(!isOpen){
			return false;
		}
		mediarecorder = new MediaRecorder();
		mediarecorder.reset();
		mediarecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);  
		mediarecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);  
		mediarecorder.setVideoSize(1920, 1080);
		mediarecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);  
		mediarecorder.setVideoFrameRate(24);
		mediarecorder.setVideoEncodingBitRate(13000000);
		mediarecorder.setPreviewDisplay(surfaceHolder.getSurface());  		
		mediarecorder.setOutputFile(fileAccess.videoLocations[nextRecord.ordinal()]);
		nextRecord = Records.values()[(nextRecord.ordinal()+1)%3];
		
		try {
			mediarecorder.prepare();			
		}
		catch (IllegalStateException e) {
			mediarecorder.reset();
			e.printStackTrace();
			return false;
		}
		catch (IOException e) {
			mediarecorder.reset();
			e.printStackTrace();
			return false;
		}
		
		try {				
			mediarecorder.start();  
		}
		catch (IllegalStateException e) {
			mediarecorder.reset();
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private synchronized void tempStop(){
		//stop recording temporarily
		if (mediarecorder != null) {  			
			mediarecorder.stop();  		
			mediarecorder.release();  
			mediarecorder = null;  
			enableButton("buttonStop", false);
			enableButton("buttonSnapshot", false);
		}  
	}
	
	private synchronized void stop(){
		//stop recording
		isOpen = false;		
		if (mediarecorder != null) {			
			mediarecorder.stop();  		
			mediarecorder.release();  
			mediarecorder = null;
		}
	}
	
	private void clean(){
		//clean history
		fileAccess.cleanTempVideoDir();
	}
	
	private synchronized void enableButton(String button, boolean enableFlag){
		if(button.equals("buttonStop")){
			if(enableFlag){
				runOnUiThread(new Runnable() 
		          {
		              public void run() 
		              {
		            	  buttonStop.setEnabled(true);
		              }
		          });
			}
			else{
				runOnUiThread(new Runnable() 
		          {
		              public void run() 
		              {
		            	  buttonStop.setEnabled(false);
		              }
		          });
			}
  		}
  		if(button.equals("buttonSnapshot")){ 
  			if(enableFlag){
				runOnUiThread(new Runnable() 
		          {
		              public void run() 
		              {
		            	  buttonSnapshot.setEnabled(true);
		              }
		          });
			}
			else{
				runOnUiThread(new Runnable() 
		          {
		              public void run() 
		              {
		            	  buttonSnapshot.setEnabled(false);
		              }
		          });
			}
  		}		
	}
	
	private synchronized ArrayList<String> snapshot(){
		Snapshot snapshot = new Snapshot();
		boolean boo = snapshot.sign(fileAccess.videoLocations, (nextRecord.ordinal()-1+fileAccess.videoLocations.length)%fileAccess.videoLocations.length);
		
		ArrayList<String> photos = new ArrayList<String>();
		if(boo){
			photos = snapshot.getPhotos();
		}		
		return photos;
	}	
	
	@Override  
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {  
		surfaceHolder = holder;  
	}  
	
	@Override  
	public void surfaceCreated(SurfaceHolder holder) {  	 
		surfaceHolder = holder;
		stopButtonTimer.schedule(new stopButtonTask(), 2000); //2sec
		snapshotButtonTimer.schedule(new snapshotButtonTask(),12500);//12.5sec
		recordTimer.schedule(new RollTask(), 0, 20000);//20sec		
	}
	
	@Override  
	public void surfaceDestroyed(SurfaceHolder holder) {  
		surfaceview = null;  
		surfaceHolder = null;
		recordTimer.cancel();
		stopButtonTimer.cancel();
		snapshotButtonTimer.cancel();
		this.stop();
	}
}