package com.example.videoapp;

/*
 * The class for gallery of certain time period. 
 * It is the second activity of gallery mode.
 */

/**
 * @author Jianghao Lu
 * Xisheng Yao
 */

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GalleryModeActivity extends Activity {		
	/**
	 * mode states which stage the user is at.
	 * When the photo is scanning and the user needs to select a photo, mode=0
	 * When the photo is selected and the user needs to select a photo option, mode=1
	 */
	private int mode = 0;
	private String activePhotoPath;
	private CountDownTimer photoTimer;
	private CountDownTimer optionTimer;
	private CountDownTimer deleteConfirmTimer;
	private int anim_pointer = 0;
	private ImageView imageView;
	private LinearLayout imageNav;
	private TextView countView;
	ArrayList<String> f = new ArrayList<String>();// list of file paths
	ArrayList<ImageView> v = new ArrayList<ImageView>(); // list of image views
	private CharSequence originalText;
	
	private Calendar from  = Calendar.getInstance();
	private Calendar to = Calendar.getInstance();	

	private static final int maxMode = 5;

	private final String CONFIRM_HEAD = "Really want to delete? Tap screen again to confirm! Timeout: ";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_gallery_mode);
	    imageView = (ImageView) findViewById(R.id.image_slideshow);
	    imageNav = (LinearLayout) findViewById(R.id.image_nav);
	    countView = (TextView) findViewById(R.id.image_cnt);    
	    
	    Bundle extras = this.getIntent().getExtras();
	    int[] fr = extras.getIntArray("from");
	    int[] tr = extras.getIntArray("to");
		from.set(fr[0], fr[1], fr[2]);
		to.set(tr[0], tr[1], tr[2]);
		Log.d("from-to", "From " + fr[0] + "-" + fr[1] + "-" + fr[2] + " to " + tr[0] + "-" + tr[1] + "-" + tr[2]);
	    
	    getFromStorage();

	    startAnimation();
	}
	
	@Override
	public void onBackPressed() {}
	
	public void startAnimation() {
		final int fileCount = f.size() + 1;
		
		photoTimer = new CountDownTimer(5000 * fileCount, 5000) {

		    public void onTick(long millisUntilFinished) {
				Bitmap bmp = BitmapFactory.decodeFile(f.get(anim_pointer));
				imageView.setImageBitmap(bmp);
				anim_pointer++;
				countView.setText(anim_pointer + "/" + f.size());
		    }

		    public void onFinish() {
		    	backToHome();
		    }
		};
		photoTimer.start();

	}
	
	public void backToHome() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		System.gc();
		finish();
	}
	
	public void screenOnClick(View view) {
		if (mode == 0) {
			/* Toggle mode */
			mode = 1;
			photoTimer.cancel();	
			
			Context context = getApplicationContext();
			CharSequence text = "You have selected a photo.";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
			
			activePhotoPath = f.get((anim_pointer-1) % f.size());

			anim_pointer = 0;
			final Button[] btns = {
					(Button) findViewById(R.id.btn_edit),
					(Button) findViewById(R.id.btn_delete),
					(Button) findViewById(R.id.btn_fb),
					(Button) findViewById(R.id.btn_email),
					(Button) findViewById(R.id.btn_return)
			};
			optionTimer = new CountDownTimer(5000 * 400, 5000) {
	
			    @SuppressWarnings("deprecation")
				public void onTick(long millisUntilFinished) {
			    	for (Button btn : btns) {
						btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.big_button));
			    	}
					Button btn = btns[anim_pointer];
					anim_pointer = (anim_pointer + 1) % 5;
					btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.active_button));
			    }
	
			    public void onFinish() {
			    	backToHome();
			    }
			};
			optionTimer.start();
		} else if (mode == 1) {
			mode = 0;
			optionTimer.cancel();
			if (anim_pointer == 1) {
				/* Edit */
		        Intent intent = new Intent(this, CroppingImageActivity.class); 
		        intent.putExtra("imgPath", activePhotoPath);
		        startActivity(intent);
				System.gc();
				finish();
			} else if (anim_pointer == 2) {
				/* Delete */
				mode = 2;
		    	originalText = countView.getText();
				deleteConfirmTimer = new CountDownTimer(10000, 1000) {

				    public void onTick(long millisUntilFinished) {
				        countView.setText(CONFIRM_HEAD + millisUntilFinished / 1000 + "s");
				    }

				    public void onFinish() {
				    	mode = 1;
				    	countView.setText(originalText);
				    	optionTimer.start();
				    }
				}.start();
			}else if(anim_pointer == 3 ){
				/* fb */
				// upload the photo to facebook
				Bitmap bmp = BitmapFactory.decodeFile(activePhotoPath);
		        Log.i("UPLOADING", "upload ...");

				uploadPicture(bmp);//call uploadpicture
				Intent intent = new Intent(this, MainActivity.class);
				startActivity(intent);
			} else if (anim_pointer == 4) {
				/* Email */
				new RetreiveFeedTask().execute(activePhotoPath);
				Intent intent = new Intent(this, MainActivity.class);
				startActivity(intent);
			} else {
				Intent intent = new Intent(this, MainActivity.class);
				startActivity(intent);
				System.gc();
				finish();
			}
		} else if (mode == 2) {
			deleteConfirmTimer.cancel();
			File toDelete = new File (activePhotoPath);
			if (!toDelete.delete()) {
				Context context = getApplicationContext();
				CharSequence text = "Delete failed!";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
			} else {
				Context context = getApplicationContext();
				CharSequence text = "Delete succeeded!";
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context, text, duration);
				toast.show();
			}
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			System.gc();
			finish();
		}
		if (mode < maxMode) {
			Log.d("+mode", Integer.toString(mode));
			mode += maxMode;
			new CountDownTimer(1000, 1000) {				
				public void onFinish() {					
					mode -= maxMode;
					Log.d("-mode", Integer.toString(mode));
				}				
				
				public void onTick(long millisUntilFinished) {}				
			}.start();
		}		
	}

	private boolean checkEquality(Calendar c1, Calendar c2) {
		if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR)) {
			return false;
		}
		if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH)) {
			return false;
		}
		if (c1.get(Calendar.DAY_OF_MONTH) != c2.get(Calendar.DAY_OF_MONTH)) {
			return false;
		}
		return true;
	}
	
	public void getFromStorage()
	{
		FileAccess fileAccess = FileAccess.getInstance();
		
		Calendar temp = from;
		temp.setLenient(true);			
		
		for (;!checkEquality(temp,to);) {
			
			List<String> pathList = fileAccess.getPhotosByDate(temp.get(Calendar.YEAR),
					temp.get(Calendar.MONTH) + 1, temp.get(Calendar.DAY_OF_MONTH), 0);
			
			f.addAll(pathList);
			
			temp.set(temp.get(Calendar.YEAR), temp.get(Calendar.MONTH),
					temp.get(Calendar.DAY_OF_MONTH) - 1);
		}
		
		for (int i = 0; i < 15 && i < f.size(); i++ ) {
            ImageView pic = new ImageView(this);
            pic.setAdjustViewBounds(true);
            
			//pic.setImageURI(Uri.fromFile(new File(f.get(i))));
            Bitmap newBmp = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(f.get(i)), 80, 80, true);
            pic.setImageBitmap(newBmp);
            imageView.setMaxHeight(80);
            imageView.setMaxWidth(80);
            pic.setScaleType(ImageView.ScaleType.FIT_XY);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(80, 80, 1);
            imageNav.addView(pic, params);
            v.add(pic);
		}
//	    File superFile= new File(STORAGE_DIR);
//	    //Here to specify folder of pictures for display 
//    	File[] listSuperFile = superFile.listFiles();
//    	for (File file : listSuperFile) {
//	        if (file.isDirectory())
//	        {
//	            listFile = file.listFiles();
//	            for (int i = 0; i < listFile.length; i++)
//	            {
//	            	if (listFile[i].getName().contains(".jpg")) {
//		                f.add(listFile[i].getAbsolutePath());
//		                ImageView pic = new ImageView(this);
//		                pic.setAdjustViewBounds(true);
//		                pic.setImageURI(Uri.fromFile(listFile[i]));
//		                imageView.setMaxHeight(80);
//		                imageView.setMaxWidth(80);
//		                pic.setScaleType(ImageView.ScaleType.FIT_XY);
//		                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(80, 80, 1);
//		                imageNav.addView(pic, params);
//		                v.add(pic);
//		                Log.d("Raw Asset: ", listFile[i].getName());
//	            	}
//	            }
//	        }
//    	}
	}

	public class ImageAdapter extends BaseAdapter {
	    private LayoutInflater mInflater;

	    public ImageAdapter() {
	        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    }

	    public int getCount() {
	        return f.size();
	    }

	    public Object getItem(int position) {
	        return position;
	    }

	    public long getItemId(int position) {
	        return position;
	    }

	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder;
	        if (convertView == null) {
	            holder = new ViewHolder();
	            convertView = mInflater.inflate(R.layout.gallery_item, null);
	            holder.imageview = (ImageView) convertView.findViewById(R.id.thumbImage);
	            convertView.setTag(holder);
	        }
	        else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        InputStream is;
			try {
				is = new FileInputStream(f.get(position));
		        new BufferedInputStream(is);
		        BitmapFactory.Options options=new BitmapFactory.Options();
		        options.inSampleSize = 8;        
		        Bitmap preview_bitmap=BitmapFactory.decodeStream(is,null,options);
		        holder.imageview.setImageBitmap(preview_bitmap);
			} catch (FileNotFoundException e) {				
				e.printStackTrace();
			}
	        return convertView;
	    }
	}
	
	class ViewHolder {
	    ImageView imageview;
	}
	
	class RetreiveFeedTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
            try {   
            	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            	String sender_email = SP.getString("example_text", "NA");
            	String sender_pw = SP.getString("example_list", "NA");
            	String receiver_email = SP.getString("example_receiver", "NA");
            	String email_subject = SP.getString("example_main_subject", "NA");
            	String email_text = SP.getString("example_main_text", "NA");
                GmailSender sender = new GmailSender(sender_email, sender_pw);
                Log.d("sender_emaiL", sender_email);
                sender.addAttachment(activePhotoPath, email_text);
                sender.sendMail(email_subject, email_text, sender_email, receiver_email);   
            } catch (Exception e) {   
    			Context context = getApplicationContext();
    			CharSequence text = "Email can't be sent. User name or password is wrong.";
    			int duration = Toast.LENGTH_SHORT;
    			Toast toast = Toast.makeText(context, text, duration);
    			toast.show();  
            }
			return null;
		}
	}
	
	//Call this function to upload a picture to facebook
	public void uploadPicture(Bitmap imageSelected){
	    Session session = Session.getActiveSession();//get session
	    
	 // Part 1: create callback to get URL of uploaded photo
	    Request.Callback uploadPhotoRequestCallback = new Request.Callback() {
			
			@Override
			public void onCompleted(Response response) {
				// TODO Auto-generated method stub
				
			}
		};
	    
		Request request = Request.newUploadPhotoRequest(session, imageSelected, uploadPhotoRequestCallback);
		request.executeAsync();	
	}
}
