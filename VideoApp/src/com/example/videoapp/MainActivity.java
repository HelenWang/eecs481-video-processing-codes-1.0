package com.example.videoapp;

/*
 * The class for home page. 
 */

/**
 * @author Jianghao Lu
 * Xisheng Yao
 * Helei Wang
 */

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
	private boolean isLocked = true;
	private Timer lockTimer = new Timer();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        
        lockTimer.schedule(new unLockTask(), 1000);
        
		FileAccess fileAccess = FileAccess.getInstance();
		fileAccess.cleanTempDir();	
    }
    
    @Override
    public void onBackPressed() {
		Intent intent = new Intent(this, HomeActivity.class);
		startActivity(intent);
    }
    
    class unLockTask extends TimerTask {		
		public void run() {			
			isLocked = false;
			lockTimer.cancel();
		}
    }
    
    public void jumpToRecord(View view) {
    	if (!isLocked) {
    		onClickRecord(view);
    	}
    }  
    
    public void onClickRecord(View view) {
        Intent intent = new Intent(this, RecordingModeActivity.class);        
        startActivity(intent);
    }
    
    public void jumpToGallery(View view) {
    	if (!isLocked) {
    		onClickGallery(view);
    	}
    } 
    
    public void onClickGallery(View view) {        
    	Intent intent = new Intent(this, GalleryPreviewActivity.class);
        startActivity(intent);
    }
     
    public void jumpToSetting(View view) {
    	if (!isLocked) {
    		onClickSetting(view);
    	}
    }
    
    public void onClickSetting(View view) {
    	Intent intent  = new Intent(this, SettingsActivity.class);
    	startActivity(intent);
    }
}