package com.example.videoapp;

/*
 * The class for Cropping function of editing photos in gallery mode
 */

/**
 * @author Jianghao Lu
 * Xisheng Yao
 */

import java.io.File;
import java.io.FileOutputStream;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nineoldandroids.animation.ObjectAnimator;

public class CroppingImageActivity extends Activity {
	/* Marks if the user has made a crop, namely to determine
	 * if a touching operation is to crop or to confirm
	 */
	private boolean cropped;
	
	private ImageView imgView;
	private ImageView borderView;
	private TextView textView;
	private int imgWidth;
	private int imgHeight;
	private int borderSize;
	/* Read in Bitmap */
	private Bitmap imgBmp;
	/* Output Bitmap */
	private Bitmap bmp;
	private float animDist;
	private String extStorageDirectory;
	private ObjectAnimator anim;
	
	private boolean isLocked = true;
	private Timer lockTimer = new Timer();
	
	private final String CONFIRM_HEAD = "Hit Screen Again to Confirm!\n Timeout: ";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cropping_image);
		
		lockTimer.schedule(new unLockTask(), 1000);
		
		cropped = false;
		imgView = (ImageView) findViewById(R.id.image_preview);
		extStorageDirectory = Environment.getExternalStorageDirectory().getPath();
		
		Intent i = getIntent();
		String impPath = i.getStringExtra("imgPath");
		imgBmp = BitmapFactory.decodeFile(impPath);
		imgView.setImageBitmap(imgBmp);
		borderView = (ImageView) findViewById(R.id.image_border);
		textView = (TextView) findViewById(R.id.prompt_msg);
	}
	
	@Override
	public void onBackPressed() {}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) { 
		super.onWindowFocusChanged(hasFocus);
		/* Adjust element sizes */
		imgWidth = imgView.getWidth();
		imgHeight = (int) (imgView.getWidth() * ((float) imgBmp.getHeight() / (float) imgBmp.getWidth()));
		imgView.getLayoutParams().height = imgHeight;
		borderSize = imgHeight;
		borderView.getLayoutParams().width = borderSize;
		borderView.getLayoutParams().height = borderSize;
		anim = null;
		startAnimation(borderView);
	}
	
	private void startAnimation(View view) {
		animDist = imgWidth - imgHeight;
		anim = ObjectAnimator.ofFloat(view,
		          "translationX", animDist);
		anim.setDuration(2000);
		anim.setRepeatCount(ObjectAnimator.INFINITE);
		anim.setRepeatMode(ObjectAnimator.REVERSE);
		anim.start();
	}
	
    class unLockTask extends TimerTask {		
		public void run() {			
			isLocked = false;
			lockTimer.cancel();
		}
    }
    
    public void onClickWrapper(View view) {
    	if (!isLocked) {
    		screenOnClick(view);
    	}
    }
	
	public void screenOnClick(View view) {
		if (cropped) {
			saveImage();
		} else {
			stopAnimation();
		}
	}
	
	private void saveImage() {
		/* Write to SD Card */
		File file = new File (extStorageDirectory, "/final_crop.jpg");
	    if (file.exists ()) file.delete (); 
	    try {
	           FileOutputStream out = new FileOutputStream(file);
	           bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	           out.flush();
	           out.close();

	    } catch (Exception e) {
	           e.printStackTrace();
	    }
	    
	    bmp.recycle();
	    imgBmp.recycle();
		
	    /* Jump to zooming activity */
		Intent intent = new Intent(this, ZoomingImageActivity.class);
		startActivity(intent);
		System.gc();
		finish();
	}
	
	private void stopAnimation() {
		if (anim != null) {
			anim.cancel();
		} else {
			return;
		}
		float animFrac = anim.getAnimatedFraction();
		
		bmp = ((BitmapDrawable)imgView.getDrawable()).getBitmap();
		int width= bmp.getWidth();
		int height= bmp.getHeight();

		/* Update Display */
		bmp=Bitmap.createBitmap(bmp, (int) ((width - height) * animFrac), 0, height, height);
		imgView.setImageBitmap(bmp);
		borderView.getLayoutParams().height = 0;
		borderView.getLayoutParams().width = 0;
		
		cropped = true;
		
		/* Confirmation */
		textView.setText(CONFIRM_HEAD + "10s");
		
		new CountDownTimer(10000, 1000) {

		    public void onTick(long millisUntilFinished) {
		        textView.setText(CONFIRM_HEAD + millisUntilFinished / 1000 + "s");
		    }

		    public void onFinish() {
				borderView.getLayoutParams().height = borderSize;
				borderView.getLayoutParams().width = borderSize;
				imgView.setImageBitmap(imgBmp);
				cropped = false;
				textView.setText("Hit Screen to Crop!");
				anim.start();
		    }
		}.start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cropping_image, menu);
		return true;
	}

}
