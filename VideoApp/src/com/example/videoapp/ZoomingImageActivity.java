package com.example.videoapp;

/*
 * The class for zooming function of editing photos in gallery mode
 */

/**
 * @author Jianghao Lu
 * Xisheng Yao
 */

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.animation.AnimatorProxy;

public class ZoomingImageActivity extends Activity {
	
	/* Marks if the user has made a crop, namely to determine
	 * if a touching operation is to crop or to confirm
	 */
	private boolean zoomed;
	
	private ImageView imgView;
	private ImageView borderView;
	private TextView textView;
	private int imgWidth;
	private int borderSize;
	/* Read in Bitmap */
	private Bitmap imgBmp;
	/* Output Bitmap */
	private Bitmap bmp;
	private String extStorageDirectory;
	private AnimatorSet anim;
	
	private boolean isLocked = true;
	private Timer lockTimer = new Timer();
	
	private final String CONFIRM_HEAD = "Hit Screen Again to Confirm!\n Timeout: ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zooming_image);
		
		lockTimer.schedule(new unLockTask(), 1000);
		
		zoomed = false;
		imgView = (ImageView) findViewById(R.id.cropped_image_preview);
		extStorageDirectory = Environment.getExternalStorageDirectory().getPath();
		imgBmp = BitmapFactory.decodeFile(extStorageDirectory + "/final_crop.jpg");
		imgView.setImageBitmap(imgBmp);
		borderView = (ImageView) findViewById(R.id.cropped_image_border);
		textView = (TextView) findViewById(R.id.cropped_prompt_msg);
	}
	
	@Override
	public void onBackPressed() {}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) { 
		super.onWindowFocusChanged(hasFocus);
		/* Adjust element sizes */
		borderSize = borderView.getHeight();
		borderView.getLayoutParams().width = borderSize;
		imgWidth = (int) (imgView.getHeight() * ((float) imgBmp.getWidth() / (float) imgBmp.getHeight()));
		imgView.getLayoutParams().width = imgWidth;
		anim = null;
		startAnimation(borderView);
	}
	
	private void startAnimation(View view) {
		AnimatorProxy.wrap(view).setPivotX((float) borderSize / 2f);
		AnimatorProxy.wrap(view).setPivotY((float) borderSize / 2f);
		//anim = ObjectAnimator.ofFloat(view, "translationX", animDist);
		anim = new AnimatorSet();
		ObjectAnimator animX = ObjectAnimator.ofFloat(view, "scaleX", 0.3f);
		ObjectAnimator animY = ObjectAnimator.ofFloat(view, "scaleY", 0.3f);
		animX.setDuration(2000);
		animX.setRepeatCount(ObjectAnimator.INFINITE);
		animX.setRepeatMode(ObjectAnimator.REVERSE);
		animY.setDuration(2000);
		animY.setRepeatCount(ObjectAnimator.INFINITE);
		animY.setRepeatMode(ObjectAnimator.REVERSE);
		anim.play(animX).with(animY);
		anim.start();
	}
	
    class unLockTask extends TimerTask {		
		public void run() {			
			isLocked = false;
			lockTimer.cancel();
		}
    }
    
    public void onClickWrapper(View view) {
    	if (!isLocked) {
    		screenOnClick(view);
    	}
    }
	
	public void screenOnClick(View view) {
		if (zoomed) {
			saveAndReturn();
		} else {
			stopAnimation();
		}
	}
	
//	private void saveImage() {
//		/* Format saving folder and fileName */
//		Time now = new Time();
//		now.setToNow();
//		String folderName = now.year + "-" + (now.month+1) + "-" + now.monthDay;
//		String fileName = now.hour + "-" + now.minute + "-" + now.second + ".jpg";
//		
//		/* Create the file */
//		File dir = new File (STORAGE_DIR, folderName);
//		if (!dir.exists()) {
//			dir.mkdirs();
//		}
//		Context context = getApplicationContext();
//		CharSequence text = "Stored in " + dir + "!";
//		int duration = Toast.LENGTH_SHORT;
//
//		Toast toast = Toast.makeText(context, text, duration);
//		toast.show();
//		File file = new File (dir, fileName);
//	    if (file.exists ()) file.delete (); 
//	    try {
//	    	/* Copy the tmp file over */
//	        FileOutputStream out = new FileOutputStream(file);
//	        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
//	        out.flush();
//	        out.close();
//	    } catch (Exception e) {
//	        e.printStackTrace();
//	    }
//	}
    
    private void saveAndReturn() {
    	FileAccess fileAccess = FileAccess.getInstance();
    	fileAccess.saveBitmap(bmp);
    	imgBmp.recycle();
    	bmp.recycle();
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		System.gc();
		finish();
    }
	
	private void stopAnimation() {
		if (anim != null) {
			anim.cancel();
		} else {
			return;
		}
		float animFrac = ((ObjectAnimator) anim.getChildAnimations().get(0)).getAnimatedFraction();

		bmp = ((BitmapDrawable)imgView.getDrawable()).getBitmap();
		int width= bmp.getWidth();
		int height= bmp.getHeight();

		/* Update Display */
		bmp=Bitmap.createBitmap(bmp, (int) (animFrac * 0.35f * width), (int) (animFrac * 0.35f * height), 
									 (int) ((1f - animFrac * 0.7) * width), (int) ((1f - animFrac * 0.7) * height));
		imgView.setImageBitmap(bmp);
		borderView.getLayoutParams().height = 0;
		borderView.getLayoutParams().width = 0;
		
		zoomed = true;
		
		/* Confirmation */
		textView.setText(CONFIRM_HEAD + "10s");
		
		new CountDownTimer(10000, 1000) {

		    public void onTick(long millisUntilFinished) {
		        textView.setText(CONFIRM_HEAD + millisUntilFinished / 1000 + "s");
		    }

		    public void onFinish() {
				borderView.getLayoutParams().height = borderSize;
				borderView.getLayoutParams().width = borderSize;
				imgView.setImageBitmap(imgBmp);
				zoomed = false;
				textView.setText("Hit Screen to Crop!");
				anim.start();
		    }
		}.start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.zooming_image, menu);
		return true;
	}

}
