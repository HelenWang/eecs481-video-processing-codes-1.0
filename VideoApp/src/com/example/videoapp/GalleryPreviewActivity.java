package com.example.videoapp;

/*
 * The class for gallery of timeline and preview. 
 * It is the first activity of gallery mode.
 */

/**
 * @author Jianghao Lu
 * Xisheng Yao
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class GalleryPreviewActivity extends Activity {	
	private final Activity myActivity = this;
	private final int[] progress = {0, 23, 45, 65, 90};
	private final int[] textViewId = {R.id.textview0, R.id.textview1, R.id.textview2, 
			R.id.textview3, R.id.textview4};
	private final int[] imageViewId = {R.id.photoPreview0, R.id.photoPreview1,
			R.id.photoPreview2, R.id.photoPreview3, R.id.photoPreview4};
	private int progressIndex = 0;
	private Handler myHandler;
	private FileAccess fileAccess = FileAccess.getInstance();
	private boolean isEmpty = true;
	private boolean isLocked = true;
	private Timer lockTimer = new Timer();
	/* from and to set the time range between which we extract photos*/
	private Calendar from = Calendar.getInstance();
	private Calendar to = Calendar.getInstance();
	private Calendar now = Calendar.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery_preview);
		lockTimer.schedule(new unLockTask(), 1000);
		from.setLenient(true);
		to.setLenient(true);	
		
		myHandler = new Handler();
		startRepeatingTask();		
	}
	
	@Override
	public void onBackPressed() {}
	
	private Runnable timeLineUpdater = new Runnable() {
		public void run() {			
			updateTimeline();
			myHandler.postDelayed(timeLineUpdater, 4000);
		}
		
		public void updateTimeline() {
			for (int i = 0; i < textViewId.length; i++) {
				TextView myTextView = (TextView) myActivity.findViewById(textViewId[i]);
				if (i == progressIndex) {					
					myTextView.setTextColor(Color.YELLOW);
				} else {
					myTextView.setTextColor(Color.LTGRAY);
				}
			}
			
			switch (progressIndex) {
				case 0:
					//Today
					from = Calendar.getInstance();
					to.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
							now.get(Calendar.DAY_OF_MONTH) - 1);
					break;
				case 1:
					//last week excludes today
					from.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
							now.get(Calendar.DAY_OF_MONTH) - 1);
					to.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
							now.get(Calendar.DAY_OF_MONTH) - 8);
					break;
				case 2:
					//last month excludes last week
					from.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
							now.get(Calendar.DAY_OF_MONTH) - 8);
					to.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH) - 1,
							now.get(Calendar.DAY_OF_MONTH) - 1);
					break;
				case 3:
					//last year excludes last month
					from.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH) - 1,
							now.get(Calendar.DAY_OF_MONTH) - 1);
					to.set(now.get(Calendar.YEAR) - 1, now.get(Calendar.MONTH),
							now.get(Calendar.DAY_OF_MONTH) - 1);
					break;
				case 4:
					//before last year
					from.set(now.get(Calendar.YEAR) - 1, now.get(Calendar.MONTH),
							now.get(Calendar.DAY_OF_MONTH) - 1);
					to.set(2010, 0, 1);			
					break;
				default:
					break;
			}
			
			for(int i = 0; i < imageViewId.length; i++) {
				ImageView myImageView = (ImageView) myActivity.findViewById(imageViewId[i]);
				myImageView.setImageDrawable(null);
			}
			
			Calendar temp = (Calendar) from.clone();
			temp.setLenient(true);
			List<String> pathList = new ArrayList<String>();			
			
			for (;!checkEquality(temp,to);) {				
				List<String> tempList = fileAccess.getPhotosByDate(temp.get(Calendar.YEAR),
						temp.get(Calendar.MONTH) + 1, temp.get(Calendar.DAY_OF_MONTH), 5);			
				
				pathList.addAll(tempList);
				
				if(pathList.size() >= 5) {
					pathList = pathList.subList(0,5);
					break;
				}					
				
				temp.set(temp.get(Calendar.YEAR), temp.get(Calendar.MONTH),
						temp.get(Calendar.DAY_OF_MONTH) - 1);
			}			
			
			isEmpty = (pathList.isEmpty()) ? true : false;
			
			for (int i = 0; i < pathList.size(); i++) {				
				ImageView myImageView = (ImageView) myActivity.findViewById(imageViewId[i]);
				myImageView.setImageURI(Uri.fromFile(new File(pathList.get(i))));
			}
			
			SeekBar mySeekBar = (SeekBar) myActivity.findViewById(R.id.seekbar0);
			mySeekBar.setProgress(progress[progressIndex]);
			progressIndex = (progressIndex + 1) % progress.length;
		}
	};
	
	private boolean checkEquality(Calendar c1, Calendar c2) {
		if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR)) {
			return false;
		}
		if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH)) {
			return false;
		}
		if (c1.get(Calendar.DAY_OF_MONTH) != c2.get(Calendar.DAY_OF_MONTH)) {
			return false;
		}
		return true;
	}
	
	private void startRepeatingTask() {
		timeLineUpdater.run();
	}
	
	private void stopRepeatingTask() {
		myHandler.removeCallbacks(timeLineUpdater);
	}
	
    class unLockTask extends TimerTask {		
		public void run() {			
			isLocked = false;
			lockTimer.cancel();
		}
    }
    
    public void jumpToGallery(View view) {
    	if (!isLocked) {
    		screenOnClick(view);    		
    	}
    }
	
	public void screenOnClick(View view) {
		if (isEmpty) {
			return;
		}
		
		stopRepeatingTask();
		Bundle myBundle = new Bundle();
		int[] tempFrom = {from.get(Calendar.YEAR), from.get(Calendar.MONTH),
				from.get(Calendar.DAY_OF_MONTH)};
		int[] tempTo = {to.get(Calendar.YEAR), to.get(Calendar.MONTH),
				to.get(Calendar.DAY_OF_MONTH)};
		
		myBundle.putIntArray("from", tempFrom);
		myBundle.putIntArray("to", tempTo);
		
		Intent intent = new Intent(this, GalleryModeActivity.class);
		intent.putExtras(myBundle);
		startActivity(intent);
		finish();
	}
}