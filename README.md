Author: Helen
		Xisheng
		Jeff
		Owen
		
It is a private repo for EECS481 course project. No one except the authors above has permission to read/write this repo or to make use of the source in the repo.

Usage:
After importing this project into Eclipse, right click the project name and open "Properties". Navigate to "Java Build Path" on the left and select "Libraries" tab on the right. Click "Add JARs" to add "NineOldAnimation.jar" in the libs folder under the project.
